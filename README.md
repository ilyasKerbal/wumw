# Who Uses My Wifi?


| Tablet |
|--|
| ![Screenshot](screenshots/tablet/tablet-new.png) |

<hr>

| Main | Main menu | Network Info | Details |
|--|--|--|--|
| ![Screenshot](screenshots/phone/main.png) | ![Screenshot](screenshots/phone/main-2.png) | ![Screenshot](screenshots/phone/network-info.png) | ![Screenshot](screenshots/phone/details.png) |

<hr>
## Stats from Google Play Console

![Stats01](screenshots/stats/stats01.jpg)

![Stats01](screenshots/stats/stats02.jpg)

![Stats01](screenshots/stats/stats03.jpg)